package de.codevise.fling;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.util.Log;

import com.amazon.whisperplay.fling.media.controller.DiscoveryController;
import com.amazon.whisperplay.fling.media.controller.RemoteMediaPlayer;
import com.amazon.whisperplay.fling.media.service.CustomMediaPlayer;
import com.amazon.whisperplay.fling.media.service.MediaPlayerStatus;
import com.google.gson.Gson;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;

public class Fling extends CordovaPlugin {
	private static final String TAG = Fling.class.getSimpleName();
	private DiscoveryController mController;
	private String serviceID = "amzn.thin.pl";
	private CallbackContext initCallbackContext = null;
	private CallbackContext mediaCallbackContext = null;
	private List<RemoteMediaPlayer> remoteMediaPlayers = new ArrayList<>();
	private RemoteMediaPlayer currentPlayer = null;

	private DiscoveryController.IDiscoveryListener mDiscovery = new DiscoveryController.IDiscoveryListener() {
		@Override
		public void playerDiscovered(RemoteMediaPlayer player) {
			if (!remoteMediaPlayers.contains(player)) {
				remoteMediaPlayers.add(player);
				sendDiscoveryChanged();
				Log.d(TAG, "### playerDiscovered");
			}
		}

		@Override
		public void playerLost(RemoteMediaPlayer player) {
			remoteMediaPlayers.remove(player);
			sendDiscoveryChanged();

			if (player.equals(currentPlayer)) {
				currentPlayer = null;
			}
			sendPlayerConnectionStatus();

			Log.d(TAG, "### playerLost");
		}

		@Override
		public void discoveryFailure() {
			Log.d(TAG, "### discoveryFailure");
		}
	};

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		if ("init".equals(action)) {
			init(args.optString(0));

			this.initCallbackContext = callbackContext;
			PluginResult result = new PluginResult(PluginResult.Status.OK);
			result.setKeepCallback(true);
			callbackContext.sendPluginResult(result);
		} else if ("isAvailable".equals(action)) {
			callbackContext.success(isAvailable());
		} else if ("playerConnected".equals(action)) {
			callbackContext.success(playerConnected());
		} else if ("getPlayerList".equals(action)) {
			callbackContext.success(getPlayerList());
		} else if ("toggleConnect".equals(action)) {
			toggleConnect();
			callbackContext.success(playerConnected());
		} else if ("initMedia".equals(action)) {
			fling(args.optString(0), args.optString(1));

			this.mediaCallbackContext = callbackContext;
			PluginResult result = new PluginResult(PluginResult.Status.OK);
			result.setKeepCallback(true);
			callbackContext.sendPluginResult(result);
		} else if ("play".equals(action)) {
			play(callbackContext);
		} else if ("pause".equals(action)) {
			pause(callbackContext);
		} else if ("stop".equals(action)) {
			stop(callbackContext);
		} else if ("seek".equals(action)) {
			seek(callbackContext, args.optLong(0));
		} else {
			Log.e(TAG, "Invalid action: " + action);
			callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.INVALID_ACTION));
			return false;
		}
		return true;
	}

	@Override
	public void initialize(final CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		mController = new DiscoveryController(this.cordova.getActivity().getApplicationContext());
	}

	@Override
	public void onResume(boolean multitasking) {
		super.onResume(multitasking);
		remoteMediaPlayers = new ArrayList<>();
		mController.start(serviceID, mDiscovery);
	}

	@Override
	public void onPause(boolean multitasking) {
		super.onPause(multitasking);
		mController.stop();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	public void init(String serviceID) {
		if (!"".equals(serviceID)) {
			this.serviceID = serviceID;
		}
		remoteMediaPlayers = new ArrayList<>();
		mController.start(this.serviceID, mDiscovery);
	}

	private void sendDiscoveryChanged() {
		sendJSONResult(initCallbackContext,
				"{deviceDiscoveryChanged:\"" + remoteMediaPlayers.size() + "\"}");
	}

	private void sendPlayerConnectionStatus() {
		boolean connected = currentPlayer != null;
		sendJSONResult(initCallbackContext, "{playerConnected:\"" + connected + "\"}");
	}

	private void sendJSONResult(CallbackContext callbackContext, String json) {
		try {
			PluginResult result = new PluginResult(PluginResult.Status.OK, new JSONObject(json));
			result.setKeepCallback(true);
			callbackContext.sendPluginResult(result);
		} catch (JSONException ignored) {
		}
	}

	private int isAvailable() {
		return remoteMediaPlayers.isEmpty() ? 0 : 1;
	}

	private JSONArray getPlayerList() {
		Collection<JSONObject> players = new ArrayList<>();

		try {
			for (int i = 0; i < remoteMediaPlayers.size(); i++) {
				RemoteMediaPlayer player = remoteMediaPlayers.get(i);
				players.add(new JSONObject("{id:" + player.getUniqueIdentifier() +
						", name:" + player.getName() + "}"));

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return new JSONArray(players);
	}

	private void toggleConnect() {
		if (currentPlayer == null) {
			if (remoteMediaPlayers.size() == 1) {
				currentPlayer = remoteMediaPlayers.get(0);
			} else if (remoteMediaPlayers.size() > 1) {
				selectRemotePlayerDialog();
			}
		} else {
			currentPlayer = null;
		}
		sendPlayerConnectionStatus();
	}

	private void selectRemotePlayerDialog() {
		Activity activity = this.cordova.getActivity();
		FakeR fakeR = new FakeR(activity);
		List<String> playerNames = new ArrayList<>();
		for (int i = 0; i < remoteMediaPlayers.size(); i++) {
			playerNames.add(i, remoteMediaPlayers.get(i).getName());
		}

		final CharSequence[] player = playerNames.toArray(new CharSequence[playerNames.size()]);

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle(fakeR.getId("string", "select_player"));
		builder.setItems(player, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				currentPlayer = remoteMediaPlayers.get(which);
			}
		});
		builder.show();
	}

	private int playerConnected() {
		return currentPlayer == null ? 0 : 1;
	}



	/* Player */

	private CustomMediaPlayer.StatusListener mListener = new Monitor();
	private static final long MONITOR_INTERVAL = 1000L;

	private void fling(final String source, final String metadataJson) {
		mStatus.source = source;
		currentPlayer.addStatusListener(mListener).getAsync(
				new ErrorResultHandler("Cannot set status listener"));
		currentPlayer.setPositionUpdateInterval(MONITOR_INTERVAL).getAsync(
				new ErrorResultHandler("Error attempting set update interval, ignoring"));
		currentPlayer.setMediaSource(source, metadataJson, true, false).getAsync(
				new ErrorResultHandler("Error attempting to Play"));
	}

	private void play(CallbackContext callbackContext) {
		if (currentPlayer != null) {
			currentPlayer.play().getAsync(getGenericFutureListener(callbackContext));
		} else {
			logNoPlayer();
		}
	}

	private void pause(CallbackContext callbackContext) {
		if (currentPlayer != null) {
			currentPlayer.pause().getAsync(getGenericFutureListener(callbackContext));
		} else {
			logNoPlayer();
		}
	}

	private void stop(CallbackContext callbackContext) {
		if (currentPlayer != null) {
			currentPlayer.stop().getAsync(getGenericFutureListener(callbackContext));
		} else {
			logNoPlayer();
		}
	}

	private void seek(CallbackContext callbackContext, Long position) {
		if (currentPlayer != null) {
			Log.d(TAG, "### SEEK TO " + position);
			currentPlayer.seek(CustomMediaPlayer.PlayerSeekMode.Absolute, position * 1000L).getAsync(
					getGenericFutureListener(callbackContext));
		} else {
			logNoPlayer();
		}
	}

	private void logNoPlayer() {
		Log.e(TAG, "No Player selected. Please select remote player first");
	}

	@NonNull
	private RemoteMediaPlayer.FutureListener<Void> getGenericFutureListener(
			final CallbackContext callbackContext) {
		return new RemoteMediaPlayer.FutureListener<Void>() {
			@Override
			public void futureIsNow(Future<Void> result) {
				try {
					result.get();
					callbackContext.success();
				} catch (Exception e) {
					callbackContext.error(e.getMessage());
				}
			}
		};
	}

	private class ErrorResultHandler implements RemoteMediaPlayer.FutureListener<Void> {
		ErrorResultHandler(String msg) {
			Log.e(TAG, msg);
		}

		@Override
		public void futureIsNow(Future<Void> result) {
			try {
				result.get();
			} catch (Exception e) {
				//handleFailure
			}
		}
	}


    /* Status */

	private Status mStatus = new Status();
	private Status oldStatus = new Status();

	private static class Status {
		public Status() {
		}

		public Status(Status status) {
			this.position = status.position;
			this.state = status.state;
			this.condition = status.condition;
			this.source = status.source;
		}

		public double position;
		public MediaPlayerStatus.MediaState state;
		public MediaPlayerStatus.MediaCondition condition;
		public String source;
	}

	private class Monitor implements CustomMediaPlayer.StatusListener {
		@Override
		public void onStatusChange(MediaPlayerStatus status, long position) {
			synchronized (mStatus) {
				mStatus.state = status.getState();
				mStatus.condition = status.getCondition();
				mStatus.position = (double) position / 1000;
			}

			if (mediaCallbackContext != null) {
				if (mStatus.equals(oldStatus)) {
					return;
				}
				Gson gson = new Gson();
				String json = gson.toJson(mStatus);
				sendJSONResult(mediaCallbackContext, json);
			}

			oldStatus = new Status(mStatus);
		}
	}
}