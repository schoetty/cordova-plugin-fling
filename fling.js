  var EventEmitter = require('cordova-plugin-fling.EventEmitter');
  var emitter = new EventEmitter();
  var fling = {};

  fling.init = function(serviceId, discoveryListener, connectionListener) {
    if (discoveryListener && typeof(discoveryListener) === 'function') {
      emitter.addListener('deviceDiscoveryChanged', discoveryListener);
    }
    if (connectionListener && typeof(connectionListener) === 'function') {
      emitter.addListener('playerConnected', connectionListener);
    }

    cordova.exec(function(success) {
        if (success.deviceDiscoveryChanged) {
          emitter.emitEvent('deviceDiscoveryChanged', [success.deviceDiscoveryChanged]);
        } else if (success.playerConnected) {
          emitter.emitEvent('playerConnected', [success.playerConnected]);
        } else {
          console.log('Fling init success');
        }
      },
      function(error) {
        console.log('Fling init error: ' + error);
      },
      "Fling", "init", [serviceId]);
  };

  fling.isAvailable = function(callback) {
    cordova.exec(function(success) {
      if (callback && typeof(callback) === 'function') {
        callback(!!success);
      }
    }, function(error) {}, "Fling", "isAvailable", []);
  };

  fling.playerConnected = function(callback) {
    cordova.exec(function(success) {
      if (callback && typeof(callback) === 'function') {
        callback(!!success);
      }
    }, function(error) {}, "Fling", "playerConnected", []);
  };

  fling.toggleConnect = function(callback) {
    cordova.exec(function(success) {
      if (callback && typeof(callback) === 'function') {
        callback(!!success);
      }
    }, function(error) {}, "Fling", "toggleConnect", []);
  };

  fling.setMediaUpdateListener = function(mediaListener) {
    emitter.removeEvent('mediaUpdate');
    if (mediaListener && typeof(mediaListener) === 'function') {
      emitter.addListener('mediaUpdate', mediaListener);
    }
  };

  fling.initMedia = function(source, metadataJson, mediaListener) {
    fling.setMediaUpdateListener(mediaListener);

    cordova.exec(function(success) {
        if (success === 'OK') {
          console.log('### mediaInit success');
        } else if (success) {
          emitter.emitEvent('mediaUpdate', [success]);
        }
      },
      function(error) {
        console.log('### mediaInit error: ' + error);
      },
      "Fling", "initMedia", [source, metadataJson]);
  };

  fling.play = function(callback) {
    cordova.exec(function(success) {
      if (callback && typeof(callback) === 'function') {
        callback(!!success);
      }
    }, function(error) {}, "Fling", "play", []);
  };

  fling.pause = function(callback) {
    cordova.exec(function(success) {
      if (callback && typeof(callback) === 'function') {
        callback(!!success);
      }
    }, function(error) {}, "Fling", "pause", []);
  };

  fling.stop = function(callback) {
    cordova.exec(function(success) {
      if (callback && typeof(callback) === 'function') {
        callback(!!success);
      }
    }, function(error) {}, "Fling", "stop", []);
  };

  fling.seek = function(position, callback) {
    cordova.exec(function(success) {
      if (callback && typeof(callback) === 'function') {
        callback(!!success);
      }
    }, function(error) {}, "Fling", "seek", [position]);
  };

  module.exports = fling;